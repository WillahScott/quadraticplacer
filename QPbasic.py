
## VLSI CAD - U. Illinois Urbana-Champaign
##     Programming Assignment 3
## Guillermo Monge - March 2015

# NOTE - must be executed from PA3, if not, base_path must be changed

__author__ = 'WillahScott'

import sys
import os.path
from QuadPlace import QuadPlace
from QPTree import QPTree


base_path = 'data/3QP/'
solution_path = 'solutions/3QP/'


def readQP(filePath):
	# Open file and read all lines
	f = open(filePath, "r")
	tx = f.readlines()
	f.close()

	# Get number of gates and nets
	num_gates, num_nets = map(int, tx[0].split())

	# Generate gates list
	gates = {}
	for i in xrange(num_gates):
		gates[i] = [0, 0] # initial position does not matter

	# Initialize nets and pads
	nets = []
	for i in xrange(num_nets):
		nets.append({'gates': set(), 'weight': 1 } )
	pads = {}

	# Read through pads first
	# num_pads = int(tx[num_gates + 1])
	net_pads = {} # dictionary: key=netID value=padID
	for ln in xrange(num_gates + 2, len(tx)):
		line = tx[ln].split()
		padID = int(line[0]) - 1
		pads[padID] = {'net': int(line[1])-1, 'pos': [int(line[2]), int(line[3])], 'weight': 1}
		net_pads[int(line[1])-1] = padID

	# Read through gates
	for j in xrange(1, num_gates + 1):
		line = tx[j].split()
		# add gate to all nets it is connected to
		for n in xrange(int(line[1])):
			nets[int(line[n+2]) - 1]['gates'].add(int(line[0]) - 1)

	# Correct pad nets into only pads
	for n, p in net_pads.iteritems():
		# add first gate to pad
		pads[p]['gate'] = nets[n]['gates'].pop()

		# should there be more, create additional pads
		while len(nets[n]['gates']) > 0:
			pads[len(pads)] = {'pos': pads[p]['pos'], 'gate': nets[n]['gates'].pop(), 'weight': 1 }

	# Delete pad nets
	nets = [nets[i] for i in xrange(len(nets)) if i not in net_pads]


	# Create QuadPlace and QPTree for the network
	root = QuadPlace(gates=gates, nets=nets, pads=pads)
	tree = QPTree(root=root)

	return tree


def writeQP(filePath, gateList):
	# open file and read all lines
	f = open(filePath, "w")

	# output each gate
	for g, pos in gateList.iteritems():
		gateLine = "%d %.8f %.8f" % (g+1, pos[0], pos[1])
		f.write(gateLine + '\n' )

	# close file
	f.close()



def main():

	if not os.path.exists(base_path):
		exit("ERROR - Base path ( %s ) not recognized. Check if code is being executed from the appropriate folder." %
		     base_path)

# I. Read INFile
	# get path of command script
	pthIN = sys.argv[1]

	if not os.path.exists(base_path + pthIN):
		exit("ERROR - File ( %s ) not found." % (base_path + pthIN) )

	network = readQP(base_path + pthIN)

	print network

	print " \nSolving network..."
	sol = network.solve(1, verbose=True)

	# Output solution
	writeQP(solution_path + 'sol_' + pthIN, sol)

if __name__ == '__main__':
	main()