from QuadPlace import QuadPlace
from QPTree import QPTree
from numpy.testing import assert_allclose
import numpy as np

__author__ = 'WillahScott'


# Initialize failed test array
failed = []
tests = 0

# Test QuadPlace object
print ' - Testing QuadPlace (node)...'
tests += 1
dummyQP = QuadPlace(gates={ 0: [0, 0], 1: [0, 0], 2: [0, 0], 3: [0, 0] },
                    nets=[{'gates': { 2, 3 }, 'weight': 10}, {'gates': { 0, 1, 2 }, 'weight': 1} ],
                    pads={ 1: {'pos': [0,0], 'gate': 1, 'weight': 1},
                           2: {'pos': [.25,1], 'gate': 3, 'weight': 20},
                           3: {'pos': [1,1], 'gate': 0, 'weight': 1} },
                    limits={ 'X': [0, 1], 'Y': [0, 1] }
                    )
print '   OK!'


# Test QPTree object
print ' - Testing QPTree...'
tests += 1
dummyTree = QPTree(dummyQP)
print '   OK!'


# Test A matrix
print ' - Testing A matrix...'
tests += 1
expectedA = np.array([ [3, -1, -1, 0], [-1, 3, -1, 0], [-1, -1, 12, -10], [0, 0, -10, 30] ])
dummyQP.compute_A_matrix()
matrixA = dummyQP.A.toarray()
try:
	assert_allclose(matrixA, expectedA)
except AssertionError:
	print '   Failed'
	failed.append('A matrix check')
else:
	print '   OK!'


# Test B vector
print ' - Testing B vector...'
tests += 1
expectedBx = np.array([1, 0, 0, 5])
expectedBy = np.array([1, 0, 0, 20])
dummyQP.compute_B_vector()
vectorBx = dummyQP.bX
vectorBy = dummyQP.bY
try:
	assert_allclose(vectorBx, expectedBx)
	assert_allclose(vectorBy, expectedBy)
except AssertionError:
	print '   Failed'
	failed.append('B vector check')
else:
	print '   OK!'


# Test QP solver
print ' - Testing QP solver...'
tests += 1
dummyQP.QPSolve()
expectedGates = [(0, .516, .842), (1, .266, .592), (2, .283, .935), (3, .261, .978)]
try:
	assert_allclose(dummyQP.solvedGates, expectedGates, atol=.001)
except AssertionError:
	print '   Failed'
	failed.append('QP solve check')
else:
	print '   OK!'


# Test assignment - overall
print ' - Testing assignment...'
tests += 1
dummyQP.assignment()

# Test assignment - gates location
print '     * Gates...'
tests += 1
try:
	assert(len(dummyQP.child1.gates) in [int(len(dummyQP.gates) / 2), int(len(dummyQP.gates) / 2) + 1])
except AssertionError:
	print '       Failed'
	failed.append('Length of Child 1\'s gates')

try:
	assert(len(dummyQP.child2.gates) in [int(len(dummyQP.gates) / 2), int(len(dummyQP.gates) / 2) + 1])
except AssertionError:
	print '       Failed'
	failed.append('Length of Child 2\'s gates')

try:
	assert( len(dummyQP.child1.gates) >= len(dummyQP.child2.gates) )
except AssertionError:
	print '       Failed'
	failed.append('Child 2 has more gates than Child 1')



## Summary
print " === Test Summary === "
print " Performed: %s tests" % tests
if len(failed) == 0:
	print " -> Passed ALL tests!"
else:
	print "  -> Passed: %d / %d : %d%%" % (tests - len(failed), tests, (tests - len(failed)) * 100. / tests)
	print "  -> Failed tests:"
	for f in failed:
		print "     +", f



print "\n\nSecond round of tests \n"

dummyQ2 = QuadPlace(gates={ 0: [0, 0], 1: [0, 0], 2: [0, 0], 3: [0, 0] },
                    nets=[{'gates': { 2, 3 }, 'weight': 10}, {'gates': { 0, 1, 2 }, 'weight': 1} ],
                    pads={ 1: {'pos': [0,0], 'gate': 1, 'weight': 1},
                           2: {'pos': [.25,1], 'gate': 3, 'weight': 20},
                           3: {'pos': [1,1], 'gate': 0, 'weight': 1},
                           4: {'pos': [1,0], 'gate': 1, 'weight': 1}},
                    limits={ 'X': [0, 1], 'Y': [0, 1] }
                    )
dummyQ2.assignment()


print "NETS"
print dummyQ2.nets
print "PADS"
print dummyQ2.pads

print "\nCHILD 1"
print str(dummyQ2.child1)
print "Gates:", dummyQ2.child1.gates.keys()
print "Nets:", dummyQ2.child1.nets
print "Pads:", dummyQ2.child1.pads

print "\nCHILD 2"
print str(dummyQ2.child2)
print "Gates:", dummyQ2.child2.gates.keys()
print "Nets:", dummyQ2.child2.nets
print "Pads:", dummyQ2.child2.pads


print "CONTAINMENT... Pads:"
dummyQ2.contain_children()
print "Pads:", dummyQ2.child1.pads
print "Pads:", dummyQ2.child2.pads


print "\n\nThird round of tests \n"

dummyQ3 = QuadPlace(gates={ 0: [0, 0], 1: [0, 0], 2: [0, 0], 3: [0, 0] },
                    nets=[{'gates': { 2, 3 }, 'weight': 10}, {'gates': { 0, 1, 2 }, 'weight': 1} ],
                    pads={ 1: {'pos': [0,0], 'gate': 1, 'weight': 1},
                           2: {'pos': [.25,1], 'gate': 3, 'weight': 20},
                           3: {'pos': [1,1], 'gate': 0, 'weight': 1},
                           4: {'pos': [1,0], 'gate': 1, 'weight': 1}},
                    limits={ 'X': [0, 1], 'Y': [0, 1] }
                    )
dummyQ3.solve_node()

assert(dummyQ2.gates == dummyQ3.gates)
assert(dummyQ2.nets == dummyQ3.nets)
assert(dummyQ2.pads == dummyQ3.pads)
assert(dummyQ2.child1.gates == dummyQ3.child1.gates)
assert(dummyQ2.child2.gates == dummyQ3.child2.gates)

"""
from unittest import TestCase

class TestQuadPlace(TestCase):
	def test_get_A_matrix(self):
		expectedA = np.array([ [3, -1, -1, 0], [-1, 3, -1, 0], [-1, -1, 12, -10], [0, 0, -10, 30] ])
		matrixA = dummyQP.get_A_matrix.toarray()
		assert_equal(matrixA, expectedA)

	def test_get_B_vector(self):
		expectedBx = np.array([1, 0, 0, 5])
		expectedBy = np.array([1, 0, 0, 20])
		vectorBx, vectorBy = dummyQP.get_B_vector
		assert_equal(vectorBx, expectedBx)
		assert_equal(vectorBy, expectedBy)
"""