
from QuadPlace import QuadPlace
from QPTree import QPTree
dummyQP = QuadPlace(gates={ 0: [0, 0], 1: [0, 0], 2: [0, 0], 3: [0, 0] },
                    nets=[{'gates': { 2, 3 }, 'weight': 10}, {'gates': { 0, 1, 2 }, 'weight': 1} ],
                    pads={ 1: {'pos': [0,0], 'gate': 1, 'weight': 1},
                           2: {'pos': [.25,1], 'gate': 3, 'weight': 20},
                           3: {'pos': [1,1], 'gate': 0, 'weight': 1} },
                    limits={ 'X': [0, 1], 'Y': [0, 1] }
                    )
dummyTree = QPTree(dummyQP)
print dummyTree

print "\n\nSolving for depth 1:"
solution = dummyTree.solve(depth=1, verbose=True)
print "\n ** SOLUTION **\n"
for g, pos in solution.iteritems():
	print " Gate %d:" % g, pos

print "\n\nSolving for depth 2:"
solution = dummyTree.solve(depth=2, verbose=True)
print "\n ** SOLUTION **\n"
for g, pos in solution.iteritems():
	print " Gate %d:" % g, pos


print "\n\nSolving for depth 1:"
solution = dummyTree.solve(depth=1, verbose=True)
print "\n ** SOLUTION **\n"
for g, pos in solution.iteritems():
	print " Gate %d:" % g, pos
