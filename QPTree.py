__author__ = 'WillahScott'

from QuadPlace import QuadPlace


class QPTree:
	"""Quadratic Placement Tree"""
	def __init__(self, root):
		assert isinstance(root, QuadPlace)
		self.root = root
		self.levels = 0
		self.solved = False
		self.levelNodes = None

	def solve(self, depth, verbose=True):
		if self.levelNodes is None:
			self.levelNodes = [[self.root]]

		# Solve as deep as required
		for i in range(len(self.levelNodes) - 1, depth):

			if verbose:
				print "+ SOLVING LEVEL %d" % i

			try:
				# get node list to solve
				unsolvedNodes = self.levelNodes[-1]

				# initialize next level nodes
				nextNodes = []

				for nd in unsolvedNodes:
					print " **", nd
					# solve node
					nd.solve_node(verbose=verbose)

					# add children to next node level
					nextNodes.append(nd.child1)
					nextNodes.append(nd.child2)

				# Add nodes to levels list
				self.levelNodes.append(nextNodes[:])

			except IndexError as e:
				print "WARNING! Not enough nodes for depth level"
				break

		## Get solution
		# initialize solution
		sol_gates = {}
		try:
			solNodes = self.levelNodes[depth]
		except IndexError:
			solNodes = self.levelNodes[-1]
			print "WARNING! No such depth, using the deepest available level: %d" % (len(self.levelNodes) - 1)
		for n_sol in solNodes:
			sol_gates.update(n_sol.gates)

		self.solved = True

		# Return solution
		return sol_gates


	def __repr__(self):
		_dict = {'ROOT': self.root, 'LEVELS': self.levels, 'SOLVED': self.solved}
		return _dict

	def __str__(self):
		if self.solved:
			status = "solved"
		else:
			status = "unsolved"
		return "Quadratic Placement Tree - %s - %s levels - the root is a %s" \
		       % (status, self.levels, str(self.root))