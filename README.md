# Quad Placer

*March 2015*

ASIC Placer implementing the Quadratic Wirelength model, as seen in the [VLSI CAD Coursera course][1], by Prof Rob A. 
Rutenbar - University of Illinois at Urbana-Champaign.  

[1]: https://www.coursera.org/course/vlsicad

### Implementation

The placer is implemented in Python 2.x.

**Dependencies**

* Python 2.6>=
* Numpy
* Scipy


### Usage

*Still Under Construction*


##### by WillahScott
