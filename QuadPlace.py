__author__ = 'WillahScott'

import numpy as np
from scipy.sparse import coo_matrix
from itertools import combinations, product
from scipy.sparse.linalg import spsolve


class QuadPlace:
	"""Quadratic Placement node"""

	def __init__(self, gates, nets, pads, limits=None, verticalCut=True):
		"""
		:type gates: dictionary: {'<gateID>': [x,y]}
		:type nets: list of dictionaries: {'gates': <set>, 'weight': 1} }
		:type pads: dictionary of dictionaries: {'<padID>': {'pos': [x,y], 'gate': <gateID>, 'weight': 1} }
		:type limits: dictionary (defaults: { 'X': [0, 100], 'Y': [0, 100] } )
		:type verticalCut: boolean (defaults True)
		"""
		self.gates = gates
		self.nets = nets
		self.pads = pads
		self.limits = limits
		if self.limits is None:
			self.limits = { 'X': [0, 100], 'Y': [0, 100] }
		self.verticalCut = verticalCut

		# Initialize cache A, b matrices
		self.A = None
		self.bX = None
		self.bY = None
		# Initialize cache solved QP positions
		self.solvedGates = None
		# Initialize children
		self.child1 = None
		self.child2 = None


	def compute_A_matrix(self):
		# initialize rows, columns and values arrays
		rows = []
		cols = []
		values = []
		diag = [0] * len(self.gates)
		for e in self.nets:
			w = e['weight']
			# adjacency matrix
			for x, y in combinations(e['gates'], 2):
				# append entry
				rows.append(self.gates.keys().index(x))
				cols.append(self.gates.keys().index(y))
				values.append(-1 * w)
				# append symmetrical entry
				rows.append(self.gates.keys().index(y))
				cols.append(self.gates.keys().index(x))
				values.append(-1 * w)
				# diagonal of matrix
				diag[self.gates.keys().index(x)] += w
				diag[self.gates.keys().index(y)] += w
		# Add pads weights to diagonal
		for p in self.pads.itervalues():
			diag[self.gates.keys().index(p['gate'])] += p['weight']

		# Combine diag into (rows, cols, values) format
		for d in xrange(len(diag)):
			v = diag[d]
			if v != 0:
				rows.append(d)
				cols.append(d)
				values.append(v)
		# Create coordinates matrix
		self.A = coo_matrix((np.array(values), (np.array(rows), np.array(cols))),
		                    shape=(len(self.gates), len(self.gates)))

	def compute_B_vector(self):
		# initialize vectors
		bx_ = [0] * len(self.gates)
		by_ = [0] * len(self.gates)
		for p,v in self.pads.iteritems():
			if type(p) is int:
				bx_[self.gates.keys().index(v['gate'])] += v['pos'][0] * v['weight']
				by_[self.gates.keys().index(v['gate'])] += v['pos'][1] * v['weight']
		# Create vectors
		self.bX = np.array(bx_)
		self.bY = np.array(by_)

	def QPSolve(self):
		if self.A is None:
			self.compute_A_matrix()
		if self.bX is None:
			self.compute_B_vector()
		# Solve for X-coords
		newX = spsolve(self.A.tocsr(), self.bX)
		# Solve for Y-coords
		newY = spsolve(self.A.tocsr(), self.bY)
		# Create solved gates positions
		self.solvedGates = zip(self.gates.keys(), newX, newY)

	def assignment(self):
		if self.solvedGates is None:
			self.QPSolve()
		# Sort gates
		if self.verticalCut:  # sort X then Y
			sortedGates = sorted(self.solvedGates, key=lambda s: (s[1], s[2]))
		else:  # sort Y then X
			sortedGates = sorted(self.solvedGates, key=lambda s: (s[2], s[1]))
		# assign to each child
		cutPoint = -1 * int(len(sortedGates) / 2)

		# gates for child 1
		_1 = sortedGates[:cutPoint]
		gates1 = {}
		for g in _1:
			gates1[g[0]] = [g[1], g[2]]

		# gates for child 2
		_2 = sortedGates[cutPoint:]
		gates2 = {}
		for g in _2:
			gates2[g[0]] = [g[1], g[2]]

		# pads for children
		pads1 = {}
		pads2 = {}
		for k, v in self.pads.iteritems():
			if v['gate'] in gates1.iterkeys():
				pads1[k] = v
			else:
				pads2[k] = v

		# nets for children
		nets1 = []
		nets2 = []
		for e in self.nets:
			# check which gates of the net are in Child 1
			inChild1 = [ g for g in e['gates'] if g in gates1.iterkeys() ]
			inChild2 = [ g for g in e['gates'] if g in gates2.iterkeys() ]

			if len(inChild2) == 0:       # all gates are in Child 1
				# add to nets1
				nets1.append(e)
			elif len(inChild1) == 0: # all gates are in Child 2
				# add to nets2
				nets2.append(e)
			else:                        # gates are mixed in Child 1 and 2
				# create pads
				w = e['weight']
				for x, y in product(inChild1, inChild2):
					pads1["g" + str(y)] = {'pos': gates2[y], 'gate': x, 'weight': w }
					pads2["g" + str(x)] = {'pos': gates1[x], 'gate': y, 'weight': w }
				# add internal sub-nets
				if len(inChild1) > 1:
					nets1.append({'gates': inChild1, 'weight': w})
				if len(inChild2) > 1:
					nets2.append({'gates': inChild2, 'weight': w})

		# compute limit midpoints for children's limits
		midX = (self.limits['X'][1] - self.limits['X'][0]) * 1. / 2 + self.limits['X'][0]
		midY = (self.limits['Y'][1] - self.limits['Y'][0]) * 1. / 2 + self.limits['Y'][0]

		if self.verticalCut:
			limits1 = { 'X': [self.limits['X'][0], midX], 'Y': [self.limits['Y'][0], self.limits['Y'][1]] }
			limits2 = { 'X': [midX, self.limits['X'][1]], 'Y': [self.limits['Y'][0], self.limits['Y'][1]] }
		else:
			limits1 = { 'X': [self.limits['X'][0], self.limits['X'][1]], 'Y': [self.limits['Y'][0], midY] }
			limits2 = { 'X': [self.limits['X'][0], self.limits['X'][1]], 'Y': [midY, self.limits['Y'][1]] }

		# Initialize child1
		self.child1 = QuadPlace(gates=gates1,
		                        nets=nets1,
		                        pads=pads1,
		                        limits=limits1,
		                        verticalCut=not self.verticalCut)
		# Initialize child2
		self.child2 = QuadPlace(gates=gates2,
		                        nets=nets2,
		                        pads=pads2,
		                        limits=limits2,
		                        verticalCut=not self.verticalCut)


	def contain_children(self):
		# contain in Child 1
		for p in self.child1.pads.itervalues():
			if self.verticalCut:
				if p['pos'][0] > self.child1.limits['X'][1]:
					p['pos'][0] = self.child1.limits['X'][1]
			else:
				if p['pos'][1] < self.child1.limits['Y'][0]:
					p['pos'][1] = self.child1.limits['Y'][0]

		for p in self.child2.pads.itervalues():
			if self.verticalCut:
				if p['pos'][0] < self.child1.limits['X'][0]:
					p['pos'][0] = self.child1.limits['X'][0]
			else:
				if p['pos'][1] > self.child1.limits['Y'][1]:
					p['pos'][1] = self.child1.limits['Y'][1]


	def solve_node(self, verbose=True):
		if self.verticalCut:
			orientation = "vertical cut"
		else:
			orientation = "horizontal cut"

		if verbose:
			print " - Solving Quadratic Placement equation"
		self.QPSolve()

		if verbose:
			print " - Running Assignment phase (%s)" % orientation
		self.assignment()

		if verbose:
			print " - Running Containment phase (%s)" % orientation
		self.contain_children()

	def __repr__(self):
		_dict = { 'LIMITS': self.limits, 'GATES': self.gates, 'NETS': self.nets, 'PADS': self.pads }
		return str(_dict)

	def __str__(self):
		return "Network with %d gates, %d nets and %d pads. The limits are %sx%s." \
		       % ( len(self.gates), len(self.nets), len(self.pads), self.limits['X'], self.limits['Y'])
